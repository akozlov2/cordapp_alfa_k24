package ru.alfabank.k24.corda.entity;

import lombok.Builder;
import lombok.Data;
import net.corda.core.serialization.CordaSerializable;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@CordaSerializable
@Data
@Builder
public class CashTransferRequestRegistry {
    private LocalDateTime date;
    private String txType;
    private BigDecimal amount;
    private List<CashTransferRequest> txDetails;
    private String status;
}