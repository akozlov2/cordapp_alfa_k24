package ru.alfabank.k24.corda.entity;

import lombok.Builder;
import lombok.Data;
import net.corda.core.serialization.CordaSerializable;

@CordaSerializable
@Data
@Builder
public class StatusReasonInfo {
    private String code;
    private String codeDescription;
}