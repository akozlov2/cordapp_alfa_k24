package ru.alfabank.k24.corda.entity;

import lombok.Builder;
import lombok.Data;
import net.corda.core.serialization.CordaSerializable;

import java.time.LocalDateTime;
import java.util.List;

@CordaSerializable
@Data
@Builder
public class CashTransferRequestTax {
    private String taxpayerKPP;
    private String taxrecipientKPP;
    private String taxOKTMO;
    private String taxPaymentNumber;
    private LocalDateTime taxPaymentDate;
    private List<CashTransferRequestTaxElement> elements;
}