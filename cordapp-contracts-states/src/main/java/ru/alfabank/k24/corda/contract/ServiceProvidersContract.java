package ru.alfabank.k24.corda.contract;

import net.corda.core.contracts.CommandData;
import net.corda.core.contracts.Contract;
import net.corda.core.transactions.LedgerTransaction;

public class ServiceProvidersContract implements Contract {
    public static final String CONTRACT_ID = "ru.alfabank.k24.corda.contract.ServiceProvidersContract";

    public interface Commands extends CommandData {
        class Issue implements CashTransferRequestRegistryContract.Commands {}
        class Settle implements CashTransferRequestRegistryContract.Commands {}
    }

    @Override
    public void verify(LedgerTransaction tx) throws IllegalArgumentException {
    }
}
