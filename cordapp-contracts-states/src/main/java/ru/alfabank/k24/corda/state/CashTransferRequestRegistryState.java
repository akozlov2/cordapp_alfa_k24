package ru.alfabank.k24.corda.state;

import net.corda.core.contracts.LinearState;
import net.corda.core.contracts.UniqueIdentifier;
import net.corda.core.identity.AbstractParty;
import net.corda.core.identity.Party;
import org.jetbrains.annotations.NotNull;
import ru.alfabank.k24.corda.entity.CashTransferRequestRegistry;

import java.util.ArrayList;
import java.util.List;

public class CashTransferRequestRegistryState implements LinearState {
    private List<AbstractParty> participants;
    private UniqueIdentifier linearId;
    private CashTransferRequestRegistry registry;

    public CashTransferRequestRegistryState(CashTransferRequestRegistry registry, List<Party> participants) {
        this.participants = new ArrayList<AbstractParty>();
        participants.forEach(p -> this.participants.add(p));
        this.registry = registry;
        linearId = new UniqueIdentifier();
    }

    @NotNull
    @Override
    public List<AbstractParty> getParticipants() {
        return participants;
    }

    @NotNull
    @Override
    public UniqueIdentifier getLinearId() {
        return linearId;
    }

    public CashTransferRequestRegistry getRegistry() {
        return registry;
    }
}