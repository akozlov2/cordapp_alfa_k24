package ru.alfabank.k24.corda.entity;

import lombok.Builder;
import lombok.Data;
import net.corda.core.serialization.CordaSerializable;

@CordaSerializable
@Data
@Builder
public class ServiceProvider {
    private int number;
    private String description;
    private String shortDescription;
    private String inn;
    private String address;
    private String kpp;
    private String ogrn;
    private AccountDetails accountDetails;
    private String component;
}