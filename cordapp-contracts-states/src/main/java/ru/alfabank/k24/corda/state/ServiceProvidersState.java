package ru.alfabank.k24.corda.state;

import net.corda.core.contracts.LinearState;
import net.corda.core.contracts.UniqueIdentifier;
import net.corda.core.identity.AbstractParty;
import net.corda.core.identity.Party;
import org.jetbrains.annotations.NotNull;
import ru.alfabank.k24.corda.entity.ServiceProvider;

import java.util.ArrayList;
import java.util.List;

public class ServiceProvidersState implements LinearState {
    private List<ServiceProvider> providers;
    private List<AbstractParty> participants;
    private UniqueIdentifier linearId;

    public ServiceProvidersState(List<ServiceProvider> providers, List<Party> participants) {
        linearId  = new UniqueIdentifier();
        this.providers = providers;
        this.participants = new ArrayList<AbstractParty>();
        participants.forEach(p -> this.participants.add(p));
    }

    @NotNull
    @Override
    public UniqueIdentifier getLinearId() {
        return linearId;
    }

    @NotNull
    @Override
    public List<AbstractParty> getParticipants() {
        return participants;
    }

    public List<ServiceProvider> getProviders() {
        return providers;
    }
}
