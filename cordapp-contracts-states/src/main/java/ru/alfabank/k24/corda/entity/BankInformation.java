package ru.alfabank.k24.corda.entity;

import lombok.Builder;
import lombok.Data;
import net.corda.core.serialization.CordaSerializable;

@CordaSerializable
@Data
@Builder
public class BankInformation {
    private String code; // BIC банка, обслуживающего счет
    private String officeBik; //БИК банка/отделения обслуживающего счет
    private String officeName; //Наименование банка/отделения обслуживающего счет
    private String officeAddress; //Адрес банка/отделения обслуживающего счет
    private String officeINN; //ИНН банка/отделения обслуживающего счет
    private String correspondentAccount;
}