package ru.alfabank.k24.corda.state;

import net.corda.core.contracts.LinearState;
import net.corda.core.contracts.UniqueIdentifier;
import net.corda.core.identity.AbstractParty;
import net.corda.core.identity.Party;
import org.jetbrains.annotations.NotNull;
import ru.alfabank.k24.corda.entity.CashTransferRequestStatusRegistry;

import java.util.ArrayList;
import java.util.List;

public class CashTransferRequestStatusRegistryState implements LinearState {
    private List<AbstractParty> participants;
    private UniqueIdentifier linearId;
    private CashTransferRequestStatusRegistry registry;

    public CashTransferRequestStatusRegistryState(CashTransferRequestStatusRegistry registry, List<Party> participants) {
        linearId = new UniqueIdentifier();
        this.participants = new ArrayList<AbstractParty>();
        participants.forEach(p -> this.participants.add(p));

        this.registry = registry;
    }

    @NotNull
    @Override
    public List<AbstractParty> getParticipants() {
        return participants;
    }

    @NotNull
    @Override
    public UniqueIdentifier getLinearId() {
        return linearId;
    }

    public CashTransferRequestStatusRegistry getRegistry() {
        return registry;
    }
}
