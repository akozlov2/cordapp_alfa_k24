package ru.alfabank.k24.corda.entity;

import net.corda.core.serialization.CordaSerializable;

@CordaSerializable
public enum TransactionGroupStatusCode {
    ACTC,
    RCVD,
    PART,
    RJCT,
    PDNG,
    ACCP,
    ACSP,
    ACSC,
    ACWC
}
