package ru.alfabank.k24.corda.entity;

import lombok.Builder;
import lombok.Data;
import net.corda.core.serialization.CordaSerializable;

import java.time.LocalDateTime;

@CordaSerializable
@Data
@Builder
public class CashTransferRequestTaxElement {
    private String taxpayerStatus;
    private String taxKBK;
    private String taxCause;
    private LocalDateTime taxYear;
    private String taxPeriodType;
    private LocalDateTime taxPeriodDateStart;
    private LocalDateTime taxPeriodDateEnd;
}