package ru.alfabank.k24.corda.contract;

import net.corda.core.contracts.CommandData;
import net.corda.core.contracts.Contract;
import net.corda.core.transactions.LedgerTransaction;

public class CashTransferRequestRegistryContract implements Contract {
    public static final String CONTRACT_ID = "ru.alfabank.k24.corda.contract.CashTransferRequestRegistryContract";

    public interface Commands extends CommandData {
        class Issue implements Commands {}
        class Settle implements Commands {}
    }

    @Override
    public void verify(LedgerTransaction tx) throws IllegalArgumentException {
    }
}