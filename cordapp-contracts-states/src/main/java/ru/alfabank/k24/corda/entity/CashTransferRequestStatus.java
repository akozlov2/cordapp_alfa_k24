package ru.alfabank.k24.corda.entity;

import lombok.Builder;
import lombok.Data;
import net.corda.core.serialization.CordaSerializable;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@CordaSerializable
@Data
@Builder
public class CashTransferRequestStatus {
    private String documentNumber;
    private BankInformation destinationBank;
    private BigDecimal amount;
    private LocalDateTime acceptanceDateTime;
    private TransactionIndividualStatusCode status;
    private List<StatusReasonInfo> statusReasonInfo;
}