package ru.alfabank.k24.corda.entity;

import net.corda.core.serialization.CordaSerializable;

@CordaSerializable
public enum TransactionIndividualStatusCode {
    ACTC,
    RJCT,
    PDNG,
    ACCP,
    ACSP,
    ACSC,
    ACWC
}
