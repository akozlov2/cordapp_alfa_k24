package ru.alfabank.k24.corda.entity;

import lombok.Builder;
import lombok.Data;
import net.corda.core.contracts.UniqueIdentifier;
import net.corda.core.serialization.CordaSerializable;

import java.util.List;

@CordaSerializable
@Data
@Builder
public class CashTransferRequestStatusRegistry {
    private List<CashTransferRequestStatus> statuses;
    private UniqueIdentifier registryFk;
    private TransactionGroupStatusCode registryStatus;
    private List<StatusReasonInfo> statusReasonInfo;
}