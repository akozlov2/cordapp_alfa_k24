package ru.alfabank.k24.corda.entity;

import lombok.Builder;
import lombok.Data;
import net.corda.core.serialization.CordaSerializable;

import java.math.BigDecimal;
import java.util.List;

@CordaSerializable
@Data
@Builder
public class CashTransferRequest {
    private String documentNumber;
    private String payerName;
    private String payerINN;
    private String payerAddress;
    private String payerAccountNumber;
    private BankInformation payerBank;
    private String recipientName;
    private String recipientINN;
    private String recipientAccountNumber;
    private BankInformation recipientBank;
    private List<String> paymentPurpose;
    private BigDecimal amount;
    private BigDecimal commission;
    private CashTransferRequestTax tax;
}