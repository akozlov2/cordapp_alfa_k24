package ru.alfabank.k24.corda.test;

import com.google.common.collect.ImmutableList;
import net.corda.testing.node.MockNetwork;
import net.corda.testing.node.StartedMockNode;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.alfabank.k24.corda.flow.CashTransferRequestRegistryFlowInitiator;

public class FlowTests {
    private MockNetwork network;
    private StartedMockNode a;
    private StartedMockNode b;

    @Before
    public void setup() {
        network = new MockNetwork(ImmutableList.of("com.template"));
        a = network.createNode();
        b = network.createNode();
        a.registerInitiatedFlow(CashTransferRequestRegistryFlowInitiator.class);
        b.registerInitiatedFlow(CashTransferRequestRegistryFlowInitiator.class);
        network.runNetwork();
    }

    @After
    public void tearDown() {
        network.stopNodes();
    }

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void test() throws Exception {

    }
}
