package ru.alfabank.k24.corda.flow;

import co.paralleluniverse.fibers.Suspendable;
import com.google.common.collect.ImmutableList;
import lombok.extern.slf4j.Slf4j;
import net.corda.core.contracts.Command;
import net.corda.core.contracts.StateAndContract;
import net.corda.core.flows.*;
import net.corda.core.identity.CordaX500Name;
import net.corda.core.identity.Party;
import net.corda.core.transactions.SignedTransaction;
import net.corda.core.transactions.TransactionBuilder;
import net.corda.core.utilities.ProgressTracker;
import ru.alfabank.k24.corda.contract.CashTransferRequestRegistryContract;
import ru.alfabank.k24.corda.entity.CashTransferRequestRegistry;
import ru.alfabank.k24.corda.state.CashTransferRequestRegistryState;

import java.security.PublicKey;
import java.util.List;

@InitiatingFlow
@StartableByRPC
@Slf4j
public class CashTransferRequestRegistryFlowInitiator extends FlowLogic<SignedTransaction> {
    private final ProgressTracker.Step GENERATING_TRANSACTION = new ProgressTracker.Step("Generating transaction based on new IOU.");
    private final ProgressTracker.Step VERIFYING_TRANSACTION = new ProgressTracker.Step("Verifying contract constraints.");
    private final ProgressTracker.Step SIGNING_TRANSACTION = new ProgressTracker.Step("Signing transaction with our private key.");
    private final ProgressTracker.Step GATHERING_SIGS = new ProgressTracker.Step("Gathering the counterparty's signature.") {
        @Override
        public ProgressTracker childProgressTracker() {
            return CollectSignaturesFlow.Companion.tracker();
        }
    };
    private final ProgressTracker.Step FINALISING_TRANSACTION = new ProgressTracker.Step("Obtaining notary signature and recording transaction.") {
        @Override
        public ProgressTracker childProgressTracker() {
            return FinalityFlow.Companion.tracker();
        }
    };

    private final ProgressTracker progressTracker = new ProgressTracker(
            GENERATING_TRANSACTION,
            VERIFYING_TRANSACTION,
            SIGNING_TRANSACTION,
            GATHERING_SIGS,
            FINALISING_TRANSACTION
    );

    @Override
    public ProgressTracker getProgressTracker() {
        return progressTracker;
    }

    private CashTransferRequestRegistry registry;

    public CashTransferRequestRegistryFlowInitiator(CashTransferRequestRegistry registry) {
        log.info("Flow CashTransferRequestRegistryFlowInitiator started");
        this.registry = registry;
    }

    @Suspendable
    @Override
    public SignedTransaction call() throws FlowException {
        Party notary = getServiceHub().getNetworkMapCache().getNotaryIdentities().get(0);

        progressTracker.setCurrentStep(GENERATING_TRANSACTION);
        Party otherParty = getServiceHub().getNetworkMapCache().getNotary(CordaX500Name.parse("O=Kvp24,L=Tolyatti,C=RU"));
        TransactionBuilder builder = new TransactionBuilder(notary);

        CashTransferRequestRegistryState outputState = new CashTransferRequestRegistryState(registry, ImmutableList.of(otherParty));
        StateAndContract outputContractAndState = new StateAndContract(outputState, CashTransferRequestRegistryContract.CONTRACT_ID);
        List<PublicKey> requiredSigners = ImmutableList.of(getOurIdentity().getOwningKey(), otherParty.getOwningKey());
        Command cmd = new Command<>(new CashTransferRequestRegistryContract.Commands.Settle(), requiredSigners);
        builder.withItems(outputContractAndState, cmd);

        progressTracker.setCurrentStep(VERIFYING_TRANSACTION);
        builder.verify(getServiceHub());

        progressTracker.setCurrentStep(SIGNING_TRANSACTION);
        SignedTransaction signedTx = getServiceHub().signInitialTransaction(builder);
        FlowSession otherpartySession = initiateFlow(otherParty);

        progressTracker.setCurrentStep(GATHERING_SIGS);
        SignedTransaction fullySignedTx = subFlow(new CollectSignaturesFlow(
                signedTx, ImmutableList.of(otherpartySession), CollectSignaturesFlow.tracker()));

        progressTracker.setCurrentStep(FINALISING_TRANSACTION);
        return subFlow(new FinalityFlow(fullySignedTx));
    }
}
