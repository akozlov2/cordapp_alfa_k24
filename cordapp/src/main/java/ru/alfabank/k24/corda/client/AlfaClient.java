package ru.alfabank.k24.corda.client;

import lombok.extern.slf4j.Slf4j;
import net.corda.client.rpc.CordaRPCClient;
import net.corda.client.rpc.CordaRPCClientConfiguration;
import net.corda.core.identity.Party;
import net.corda.core.messaging.CordaRPCOps;
import net.corda.core.messaging.DataFeed;
import net.corda.core.node.NodeInfo;
import net.corda.core.node.services.Vault;
import net.corda.core.utilities.NetworkHostAndPort;
import org.apache.activemq.artemis.api.core.ActiveMQException;
import ru.alfabank.k24.corda.entity.CashTransferRequestRegistry;
import ru.alfabank.k24.corda.flow.CashTransferRequestRegistryFlowInitiator;
import ru.alfabank.k24.corda.state.CashTransferRequestRegistryState;
import rx.Observable;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.concurrent.ExecutionException;

@Slf4j
public class AlfaClient {

    public static void main(String[] args) throws ActiveMQException, InterruptedException, ExecutionException {
        final NetworkHostAndPort nodeAddress = NetworkHostAndPort.parse(args[0]);
        final CordaRPCClient client = new CordaRPCClient(nodeAddress, CordaRPCClientConfiguration.DEFAULT);

        final CordaRPCOps proxy = client.start("user1", "test").getProxy();

        final DataFeed<Vault.Page<CashTransferRequestRegistryState>, Vault.Update<CashTransferRequestRegistryState>> dataFeed =
                proxy.vaultTrack(CashTransferRequestRegistryState.class);

        final Vault.Page<CashTransferRequestRegistryState> snapshot = dataFeed.getSnapshot();
        final Observable<Vault.Update<CashTransferRequestRegistryState>> updates = dataFeed.getUpdates();

        for(NodeInfo ni : proxy.networkMapSnapshot()) {
            for (Party p : ni.getLegalIdentities()) {
                log.info("party name: {}", p.getName());
            }
            for(NetworkHostAndPort nhap : ni.getAddresses()) {
                log.info("party host {} and port {}", nhap.getHost(), nhap.getPort());
            }
        }

        proxy.notaryIdentities().stream().forEach(p -> log.info("notary: {}", p.getName()));
        proxy.registeredFlows().stream().forEach(f -> log.info("flow: {}", f));
        CashTransferRequestRegistry registry = CashTransferRequestRegistry.builder()
                .amount(new BigDecimal(10))
                .date(LocalDateTime.now())
                .status("test")
                .build();
        proxy.startFlowDynamic(CashTransferRequestRegistryFlowInitiator.class, registry);

        snapshot.getStates().forEach(s -> log.info("snapshot: {}", s.getState().getData().getRegistry()));
        updates.toBlocking().subscribe(update -> update.getProduced().forEach(u -> log.info("updater: {}", u.getState().getData().getLinearId())));
    }
}