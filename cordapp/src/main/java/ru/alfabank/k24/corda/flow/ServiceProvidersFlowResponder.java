package ru.alfabank.k24.corda.flow;

import co.paralleluniverse.fibers.Suspendable;
import net.corda.core.flows.*;
import net.corda.core.transactions.SignedTransaction;
import org.jetbrains.annotations.NotNull;

@InitiatedBy(ServiceProvidersFlowInitiator.class)
public class ServiceProvidersFlowResponder extends FlowLogic<SignedTransaction> {
    private FlowSession counterpartySession;

    public ServiceProvidersFlowResponder(FlowSession counterpartySession) {
        this.counterpartySession = counterpartySession;
    }

    @Suspendable
    @Override
    public SignedTransaction call() throws FlowException {
        return subFlow(new SignTransactionFlow(counterpartySession, SignTransactionFlow.Companion.tracker()) {
            @Override
            protected void checkTransaction(@NotNull SignedTransaction stx) throws FlowException {

            }
        });
    }
}